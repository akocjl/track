package com.jl.track.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.jl.track.R;
import com.jl.track.rest.ApiManager;
import com.jl.track.rest.ApiService;
import com.jl.track.rest.httpmodels.ResponseStations;
import com.jl.track.rest.httpmodels.ResponseTracks;
import com.jl.track.rest.models.Hello;
import com.jl.track.rest.models.ResponseVehicles;
import com.jl.track.rest.models.Station;
import com.jl.track.rest.models.Vehicle;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {

    private GoogleMap mMap;

    private DrawerLayout drawer;
    private View drawerView;
    private ActionBarDrawerToggle drawerToggle;
    private ApiService apiManager;

    private ButteryProgressBar progress;

    private HashMap<Integer, Circle> stationMarkers;

    private int currentTrackId = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpToolBar();
        setUpMapIfNeeded();

        progress = (ButteryProgressBar) findViewById(R.id.progress);
        stationMarkers = new HashMap<Integer, Circle>();
        apiManager = ApiManager.getTrackService();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_refresh);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                showProgress();
                getTrack(currentTrackId);
                return true;
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(drawerView)) {
            drawer.closeDrawer(drawerView);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }


    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("tracker");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerView = findViewById(R.id.left_drawer);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(drawerToggle);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            MapFragment frag = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mMap = frag.getMap();

            // Check if we were successful in obtaining the map.
            if (mMap != null)
                setUpMap();
            // The Map is verified. It is now safe to manipulate the map.
        }
    }

    private void setUpMap() {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(14.555461, 121.023266), 14);
        mMap.animateCamera(cameraUpdate);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        // mMap.setOnMarkerClickListener(this);
        // mMap.setInfoWindowAdapter(new GeneralWindowAdapter());
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
    }

    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void onAddTrack(View view) {
        showMessage("add");
        drawer.closeDrawer(drawerView);



        /**
        apiManager.getAllTracks(new Callback<ResponseTracks>() {
            @Override
            public void success(ResponseTracks responseTracks, Response response) {
                Log.i("track count: ", responseTracks.getTracks().size() + "");
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
         */

        /**
        apiManager.getVehicles(2, new Callback<ResponseVehicles>() {
            @Override
            public void success(ResponseVehicles responseVehicles, Response response) {
                Log.i("vehicle count: ", responseVehicles.getVehicles().size() + "");
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
         */
        //showProgress();
        //getTrack(1);

    }

    private void plotStations(List<Station> stations){
        mMap.clear();

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();

        for (Station s : stations) {
            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng(s.getLatitude(), s.getLongitude()))
                    .radius(15)
                    .strokeColor(strokeColor)
                    .fillColor(shadeColor);

            Circle c = mMap.addCircle(circleOptions);
            stationMarkers.put(s.getStopId(), c);
            boundsBuilder.include(new LatLng(s.getLatitude(), s.getLongitude()));
        }
        // TODO: plot route
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 100));

    }

    private void plotVehicles(List<Vehicle> vehicles) {
        for (Vehicle v : vehicles) {
            Circle station = stationMarkers.get(v.getStopId());

            Marker vehicle = mMap.addMarker(new MarkerOptions()
                    .position(station.getCenter())
                    .title(v.getName()));
        }
    }

    private void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progress.setVisibility(View.INVISIBLE);
    }

    private void getTrack(final int trackId) {
        currentTrackId = trackId;
        apiManager.getStations(trackId, new Callback<ResponseStations>() {
            @Override
            public void success(ResponseStations responseStations, Response response) {
                Log.i("station count: ", responseStations.getStations().size() + "");
                plotStations(responseStations.getStations());

                apiManager.getVehicles(trackId, new Callback<ResponseVehicles>() {
                    @Override
                    public void success(ResponseVehicles responseVehicles, Response response) {
                        Log.i("vehicle count: ", responseVehicles.getVehicles().size() + "");
                        plotVehicles(responseVehicles.getVehicles());
                        hideProgress();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    int strokeColor = 0xff0000ff;
    int shadeColor = 0xff0000ff;

    public void shoTrackOne(View view) {
        drawer.closeDrawer(drawerView);
        showProgress();
        getSupportActionBar().setTitle("Makati E-jeep White");
        getTrack(1);
    }

    public void showTrackTwo(View view) {
        drawer.closeDrawer(drawerView);
        showProgress();
        getSupportActionBar().setTitle("Makati E-jeep Green");
        getTrack(2);
    }
}
