package com.jl.track.rest;

import retrofit.RestAdapter;

/**
 * Created by JL on 12/7/2014.
 */
public class ApiManager {

    private static final String BASE_URL = "http://demo6143431.mockable.io";

    private static final RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(BASE_URL)
            .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
            .build();

    private static final ApiService trackService = restAdapter.create(ApiService.class);

    public static ApiService getTrackService(){
        return trackService;
    }

}
