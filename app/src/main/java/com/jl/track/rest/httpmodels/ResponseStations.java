package com.jl.track.rest.httpmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jl.track.rest.models.Station;

import java.util.ArrayList;
import java.util.List;

public class ResponseStations {

    @SerializedName("track_id")
    @Expose
    private Integer trackId;
    @Expose
    private List<Station> stations = new ArrayList<Station>();

    /**
     * @return The trackId
     */
    public Integer getTrackId() {
        return trackId;
    }

    /**
     * @param trackId The track_id
     */
    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    /**
     * @return The stations
     */
    public List<Station> getStations() {
        return stations;
    }

    /**
     * @param stations The stations
     */
    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

}