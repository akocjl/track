package com.jl.track.rest.httpmodels;

import com.google.gson.annotations.Expose;
import com.jl.track.rest.models.Track;

import java.util.ArrayList;
import java.util.List;

public class ResponseTracks {

    @Expose
    private List<Track> tracks = new ArrayList<Track>();

    /**
     * @return The tracks
     */
    public List<Track> getTracks() {
        return tracks;
    }
}