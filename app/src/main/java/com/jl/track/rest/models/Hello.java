package com.jl.track.rest.models;

import com.google.gson.annotations.Expose;

public class Hello {

    @Expose
    private String msg;

    /**
     * @return The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

}