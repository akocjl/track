package com.jl.track.rest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Track {

    @Expose
    private String name;
    @SerializedName("track_id")
    @Expose
    private Integer trackId;
    @Expose
    private List<Station> stations = new ArrayList<Station>();

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The trackId
     */
    public Integer getTrackId() {
        return trackId;
    }

    /**
     * @param trackId The track_id
     */
    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    /**
     * @return The stations
     */
    public List<Station> getStations() {
        return stations;
    }

    /**
     * @param stations The stations
     */
    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

}