package com.jl.track.rest;

import com.jl.track.rest.httpmodels.ResponseStations;
import com.jl.track.rest.httpmodels.ResponseTracks;
import com.jl.track.rest.models.Hello;
import com.jl.track.rest.models.ResponseVehicles;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface ApiService {

    @GET("/tracks")
    void getAllTracks(Callback<ResponseTracks> callback);

    @GET("/vehicles/{trackId}")
    void getVehicles(@Path("trackId") int id, Callback<ResponseVehicles> callback);

    @GET("/stops/{trackId}")
    void getStations(@Path("trackId") int id, Callback<ResponseStations> callback);

}
