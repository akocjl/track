package com.jl.track.rest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vehicle {

    @Expose
    private String name;
    @SerializedName("track_id")
    @Expose
    private Integer trackId;
    @SerializedName("stop_id")
    @Expose
    private Integer stopId;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The trackId
     */
    public Integer getTrackId() {
        return trackId;
    }

    /**
     * @param trackId The track_id
     */
    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    /**
     * @return The stopId
     */
    public Integer getStopId() {
        return stopId;
    }

    /**
     * @param stopId The stop_id
     */
    public void setStopId(Integer stopId) {
        this.stopId = stopId;
    }

}